#include <cstdint>
#include <array>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include <TROOT.h>
#include <TGraph.h>
#include "TTree.h"
#include "TTreeFormula.h"
#include "TH1F.h"
#include "TMinuit.h"
#include "TFile.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "THStack.h"
#include "TLegend.h"
#include "TChain.h"
#include "TFile.h"
#include "TH2.h"
#include "TH2.h"
#include "TH3.h"
#include "THn.h"
#include "TCanvas.h"
#include "TLatex.h"

#include "TH1F.h"
#include "TDirectory.h"
#include "TDirectoryFile.h"
#include "TArrow.h"
#include "TRef.h"
#include "TApplication.h"
#include "TError.h"
#include "TMath.h"
#include "TAxis.h"
#include "TStyle.h"
#include "TLine.h"
#include "TMinuit.h"

#include "TVirtualFitter.h"
#include "TMath.h"
#include "TStopwatch.h"

//#include "ATLASStyle/AtlasStyle.C"
//#include "ATLASStyle/AtlasLabels.C"
//#include "ATLASStyle/AtlasUtils.C"
class Singleton {
public:
    double test_value;
    double Dyz2, Dxz2;
    Int_t s_Z[64];
    Int_t s_XY[64];
    Double_t s_drift_radius[64];
private:
  static Singleton* instance;
  Singleton(){
    test_value = 1.0;
    Dyz2 = 0.0, Dxz2 = 0.0;
  }
  virtual ~Singleton(){}

public:
  static Singleton* getInstance(){
    if (!instance) instance = new Singleton();
    return instance;
  }

  double getvalue() {
      return this -> test_value;
  }
  void setvalue(double newv) {
      this -> test_value = newv;
  }

};

class tran{
public:

  void plots(); 
  //npar->number of free parameters involved in minimization; gin->computed gradient values; 
  //f->the function value itself; par->vector of constant and variable parameters
  //flag->to switch between several actions of FCN
  static void D2funcYZ(Int_t &, Double_t *, Double_t &f, Double_t *par, Int_t iflag);
  static void D2funcXZ(Int_t &, Double_t *, Double_t &f, Double_t *par, Int_t iflag);

private:
  Int_t Z[64];
  Int_t XY[64];
  Double_t drift_radius[64];
  Int_t tdc[64][10]; 
  Int_t adc[64][10]; 
  Int_t delta_tdc[64][10];
  Double_t coefficient[5]; 
  Double_t fiterror[5]; 
  Double_t timer = 0; 
  int nentries1 = 0;
  int flag_signal = 0;
  int ch_count[6];
  double x_final = 0;

  double coef[6];
  double coefH[2];


  TFile* file;
  TFile* plot_file;
  TTree* mytree;
  TTree *parameter_tree;

  double runtime;

  // === fit ===
  std::string par1Name,par2Name,par3Name;
  double stepSize;
  double init_value;
  double fit_min;
  double fit_max;
  double arglist[3];
  double par0, par1, par2;
  double par0_err, par1_err, par2_err;
  int ierflg;
//  TMinuit *min;
//  TStopwatch timer;
  
};
